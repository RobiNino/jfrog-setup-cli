#!/usr/bin/env bats
#
# Test JFrog Setup CLI
#

set -e

setup() {
  # Build image:
  DOCKER_IMAGE=${DOCKER_IMAGE:="test/jfrog-setup-cli"}
  run docker build -t ${DOCKER_IMAGE} ./
}

@test "Test Setup" {
  # Execute:
  run docker run \
    -v $(pwd):$(pwd) \
    -w $(pwd) \
  ${DOCKER_IMAGE}
  checkStatus

  source ./jfrog-setup-cli.sh
  checkStatus

  # Run test commands:
  runCmd jf -v
  runCmd jf rt ping
  runCmd jf rt u README* generic-local
  runCmd jf rt dl generic-local/README.md readmedl
  runCmd jf rt bce
  runCmd jf rt bp

  verifyBuild 1 1
}

# Run command, print and check output and status
runCmd() {
  echo -e "\nRunning Command: $@" >&3
  run "$@"
  checkStatus
}

checkStatus() {
  echo "Status: $status" >&3
  echo "Output: $output" >&3
  [ "$status" -eq 0 ]
}

# Verify published build has $1 artifact and $2 dependencies
verifyBuild() {
  echo "Verifying build..." >&3
  run jf rt curl -XGET "/api/build/$JFROG_CLI_BUILD_NAME/$JFROG_CLI_BUILD_NUMBER" -o buildInfo
  [ "$status" -eq 0 ]

  ARTIFACTS_NUM=$(cat buildInfo | jq '.buildInfo.modules[0].artifacts | length')
  echo "Expected $1 artifacts, found $ARTIFACTS_NUM" >&3
  [ "$ARTIFACTS_NUM" -eq $1 ]

  DEPENDENCIES_NUM=$(cat buildInfo | jq '.buildInfo.modules[0].dependencies | length')
  echo "Expected $2 dependencies, found $DEPENDENCIES_NUM" >&3
  [ "$DEPENDENCIES_NUM" -eq $2 ]
}