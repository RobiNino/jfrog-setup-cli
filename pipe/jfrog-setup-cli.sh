#!/bin/bash
set -e

# Pipe version - This field is changed by the CI which expects this exact syntax. AVOID changing manually.
PIPE_VERSION="2.0.0"

DEFAULT_CLI_VERSION="2.56.1"

# Get custom CLI version if set, or default if doesn't.
JFROG_CLI_VERSION=${JFROG_CLI_VERSION:=$DEFAULT_CLI_VERSION}

# If the JFROG_CLI_VERSION was set to 'latest', resolve to an empty string.
if [ "$JFROG_CLI_VERSION" = "latest" ]; then
    JFROG_CLI_VERSION=""
fi

# Download and Install JFrog CLI
curl -fL https://install-cli.jfrog.io | sh -s $JFROG_CLI_VERSION

# Verify CLI was downloaded
if [ ! -f $(which jf) ]; then
    echo "JFrog CLI Installation failed."
    exit 1
fi

# Loop and import all config tokens
while IFS='=' read -r name value ; do
  if [[ $name == 'JF_ENV_'* ]] || [[ $name == 'JF_ARTIFACTORY_'* ]]; then
    jf config import ${!name}
  fi
done < <(env)

# Export env var
export JFROG_CLI_BUILD_NAME=${JFROG_CLI_BUILD_NAME:=$BITBUCKET_REPO_OWNER-$BITBUCKET_REPO_SLUG-$BITBUCKET_BRANCH}
export JFROG_CLI_BUILD_NUMBER=${JFROG_CLI_BUILD_NUMBER:=$BITBUCKET_BUILD_NUMBER}
export JFROG_CLI_BUILD_URL=${JFROG_CLI_BUILD_URL:="https://bitbucket.org/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}/addon/pipelines/home#!/results/${BITBUCKET_BUILD_NUMBER}"}
export JFROG_CLI_USER_AGENT="bitbucket-pipelines/${PIPE_VERSION}"

# Create alias of deprecated jfrog -> jf
alias jfrog=jf

# Delete this script after it's done executing
rm ./jfrog-setup-cli.sh