# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 2.0.0

- major: Promote JFrog CLI to v2

## 1.0.0

- major: Initial Release
